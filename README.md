# Trending GitHub repositories

Android Code Challenge
This is an evaluation project that tries to identify the coding compatibility with the team. Try
to code as clean and clear as possible, taking into account reusability and legibility is more
important than performance.
What to do:
The main idea of the project is to build a native Android app that has 2 screens,
1. A list of trending GitHub repositories of Android
2. On tapping a repository from the list, the app should open a simple “Detailed View”
with more information.
