package com.nayan.varun.ui.repo_list

import com.nayan.varun.data.model.Repo

interface GitRepoListView {

    fun showLoader()
    fun hideLoader()
    fun onRepoFetchSuccess(repos: ArrayList<Repo>)
    fun onRepoFetchFailure(throwable: Throwable?)

}