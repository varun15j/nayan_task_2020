package com.nayan.varun.data.remote

import com.nayan.varun.data.model.response.RepoResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("search/repositories?q=android+language:java&sort=stars&order=desc")
    fun fetchRepos(): Call<RepoResponse>
}